FROM registry.gitlab.com/querl.dox/jupyter/build/focal:latest

ENV GH_REPO https://github.com/rust-lang/rustlings
ENV GH_DIR /tmp/$CI_PROJECT_NAME
ENV RUSTLINGS_VER 2.0.0

RUN DEBIAN_FRONTEND=nointeractive apt-get update && apt-get install -y --no-install-recommends \
    rustc cargo \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

RUN git clone $GH_REPO $GL_DIR
WORKDIR $GH_DIR
RUN git checkout 2.0.0
RUN cargo install --force --path .